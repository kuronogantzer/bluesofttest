# BlueSoftTest
---
### 1. Requisitos y tecnologías
- **Base de datos:** SQL Server Express
- **Backend:** ASP.NET Core 3 con Entity Framework Core
- **Frontend:** Angular 8

### 2. Instalación y configuración de entorno

- Tras clonar el repositorio ingresar al directorio `Library`y ejecutar el comando:

```
$ dotnet restore
```

- Ingresar al siguiente directorio `Library`, abrir el archivo `appsettings.json` y modificar la propiedad `connectionString` por los datos de conexión de la base de datos a usar.

```
Server=DESKTOP-R0BSF80\\SQLEXPRESS;Database=Library;User Id=sa;Password=123456;MultipleActiveResultSets=true
```

- Ahora ejecutar el siguiente comando para crear las tablas de la base de datos:

```
$ dotnet ef database update
```

- Tras crear las tablas el API se puede ejecutar con el comando:
```
$ dotnet run
```

- Para ejecutar la aplicación del Frontend hecha en Angular 8 es necesario ingresar al directorio `ClientApp` y ejecutar los siguientes comandos:

```
$ npm install
$ npm start
```

### 3. Arquitectura y Funcionalidades

La aplicación tiene 2 componenes principales que se ejecutan de forma independiente:

- **Aplicación de servidor:** Aplicación hecha con ASP.NET Core 3 que se compone de 4 capas:
    - **Models:** Contiene los modelos y entidades utilizados en toda la aplicación además de la clase `LibraryContext` que administra la comunicación con la base de datos a través de Entity Framework.
    - **Repositories:** Contiene las operaciones CRUD asociadas a las entidades de negocio de la aplicación (Libros, Autores y Categorias). Éstas operaciones se realizan usando instancias de `LibraryContext`.
    - **Business:** Contiene las validaciones, lógica y operaciones de negocio realizando llamados a la capa de **Repositories**.
    - **Controllers:** Contiene los endpoints que se exponen a através del API REST para cada entidad de negocio de la aplicación en la ruta `http://localhost:5000/{entidad}`.

La aplicación de servidor cuenta además con una interfaz gráfica que permite conocer y probar los detalles de cada endpoint expuesto por el API REST en la ruta `http://localhost:5000/swagger/index.html`.

- **Aplicacion cliente:** Aplicación hecha en Angular 8 con el módulo `app` en el que se encuentran separados los **componentes**, **servicios** y **modelos** y que se levanta sobre la ruta `http://localhost:5001`