﻿using Autofac;
using LibraryBusiness;
using LibraryEntities;
using LibraryRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Library
{
    public class IoCContainer
    {
        public static void ConfigureContainer(ContainerBuilder builder, IConfiguration configuration)
        {

            builder.Register((param, context) => new DbContextOptions<LibraryContext>())
                .As<DbContextOptions<LibraryContext>>();

            builder.Register((context, param)
                    => new LibraryContext(context.Resolve<DbContextOptions<LibraryContext>>(),
                        configuration["SQLServer:ConnectionString"]))
                .As<LibraryContext>().InstancePerLifetimeScope();

            builder.Register((context, param) 
                    => new CategoryRepository(context.Resolve<LibraryContext>()))
                .As<CategoryRepository>();

            builder.Register((context, param)
                    => new AuthorRepository(context.Resolve<LibraryContext>()))
                .As<AuthorRepository>();

            builder.Register((context, param)
                    => new BookRepository(context.Resolve<LibraryContext>()))
                .As<BookRepository>();

            builder.Register((context, param)
                    => new CategoryBusiness(context.Resolve<CategoryRepository>()))
                .As<CategoryBusiness>();

            builder.Register((context, param)
                    => new AuthorBusiness(context.Resolve<AuthorRepository>()))
                .As<AuthorBusiness>();

            builder.Register((context, param)
                    => new BookBusiness(context.Resolve<BookRepository>()))
                .As<BookBusiness>();

        }
    }
}