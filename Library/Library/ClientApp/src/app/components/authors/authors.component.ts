import { Component, OnInit } from '@angular/core';
import {Result} from "../../models/LibraryResponse";
import {ToasterService} from "angular2-toaster";
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import {AuthorService} from "../../services/author.service";
import {Author} from "../../models/Author";

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styles: []
})
export class AuthorsComponent implements OnInit {

  private authors: Author[];
  private notFound: boolean;
  private editIcon = faEdit;
  private deleteIcon = faTrash;

  constructor(private _service: AuthorService,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this._service.GetAuthors().subscribe( result => {
      if (result.Result == Result.SuccessfulOperation)
        this.authors = result.Data;
      else {
        this._toasterService.pop({
          type: 'error',
          title: result.Message
        });
      }
    }, error => {
      if (error.status !== 404) {
        this._toasterService.pop({
          type: 'error',
          title: error.error.Message
        });
      }
      else {
        this.authors = undefined;
        this.notFound = error.status === 404;
      }
    });
  }

  delete(idAuthor: number) {
    this._service.DeleteAuthor(idAuthor)
      .subscribe( resp => {
        if (resp.Result === Result.SuccessfulOperation) {
          this._toasterService.pop({
            type: 'success',
            title: 'Autor eliminado'
          });
          this.ngOnInit();
        }
      }, error => {
        console.log(error);
        this._toasterService.pop({
          type: 'error',
          title: 'Ocurrió un error al eliminar el autor'
        });
      })
  }

}
