import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {Result} from "../../../models/LibraryResponse";
import {Author} from "../../../models/Author";
import {AuthorService} from "../../../services/author.service";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import * as moment from 'moment';

@Component({
  selector: 'app-author-detail',
  templateUrl: './author-detail.component.html',
  styles: []
})
export class AuthorDetailComponent implements OnInit {

  private authorForm: FormGroup;
  private edit: boolean;
  private author: Author;
  private validated: boolean;
  private maxDate: string;
  private calendarIcon = faCalendar;

  constructor(private _service: AuthorService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _formBuilder: FormBuilder,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this.maxDate = moment(new Date()).format('DD/MM/YYYY');
    this.authorForm = this._formBuilder.group({
      Name: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      BirthDate: new FormControl(Date.now(), Validators.required)
    });

    this._activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.edit = true;
        this._service.GetAuthorById(+params['id'])
          .subscribe(result => {
            this.author = result.Data;
            this.author.BirthDate = this.fromJsonDate(this.author.BirthDate);
            this.authorForm.patchValue(this.author);
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'No se pudo obtener la información del autor.'
            });
          });
      }
    });
  }

  save() {
    if (this.authorForm.valid) {
      this.author = this.edit ? this.author : new Author();
      this.author.Name = this.authorForm.value.Name;
      this.author.LastName = this.authorForm.value.LastName;
      this.author.BirthDate = this.authorForm.value.BirthDate;
      if (this.edit) {
        this._service.UpdateAuthor(this.author)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Autor actualizado'
              });
              this._router.navigate(['/autores']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al actualizar el autor.'
            });
          });
      } else {
        this._service.CreateAuthor(this.author)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Autor creado'
              });
              this._router.navigate(['/autores']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al crear el autor'
            });
            this._router.navigate(['/autores']);
          });
      }
    }
    else {
      this.validated = true
    }
  }

  clear() {
    this.authorForm.reset();
  }

  fromJsonDate(jDate): string {
    const bDate: Date = new Date(jDate);
    return bDate.toISOString().substring(0, 10);  //Ignore time
  }
}
