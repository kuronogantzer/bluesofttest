import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from "../../../services/category.service";
import {Category} from "../../../models/Category";
import {ToasterService} from 'angular2-toaster';
import {Result} from "../../../models/LibraryResponse";

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styles: []
})
export class CategoryDetailComponent implements OnInit {

  private categoryForm: FormGroup;
  private edit: boolean;
  private category: Category;
  private validated: boolean;

  constructor(private _service: CategoryService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _formBuilder: FormBuilder,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this.categoryForm = this._formBuilder.group({
      Name: new FormControl('', Validators.required),
      Description: new FormControl('', Validators.required)
    });

    this._activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.edit = true;
        this._service.GetCategoryById(+params['id'])
          .subscribe(result => {
            this.category = result.Data;
            this.categoryForm.patchValue(this.category);
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'No se pudo obtener la información de la categoría.'
            });
          });
      }
    });
  }


  save() {
    if (this.categoryForm.valid) {
      this.category = this.edit ? this.category : new Category();
      this.category.Name = this.categoryForm.value.Name;
      this.category.Description = this.categoryForm.value.Description;
      if (this.edit) {
        this._service.UpdateCategory(this.category)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Categoría actualizada'
              });
              this._router.navigate(['/categorias']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al actualizar la categoría.'
            });
          });
      } else {
        this._service.CreateCategory(this.category)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Categoría creada'
              });
              this._router.navigate(['/categorias']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al crear la categoría.'
            });
            this._router.navigate(['/categorias']);
          });
      }
    }
    else {
      this.validated = true
    }
  }

  clear() {
    this.categoryForm.reset();
  }
}
