import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../models/Category";
import {Result} from "../../models/LibraryResponse";
import {ToasterService} from "angular2-toaster";
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styles: []
})
export class CategoriesComponent implements OnInit {

  private categories: Category[];
  private notFound: boolean;
  private editIcon = faEdit;
  private deleteIcon = faTrash;

  constructor(private _service: CategoryService,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this._service.GetCategories().subscribe( result => {
      if (result.Result == Result.SuccessfulOperation)
        this.categories = result.Data;
      else {
        this._toasterService.pop({
          type: 'error',
          title: result.Message
        });
      }
    }, error => {
      if (error.status !== 404) {
        this._toasterService.pop({
          type: 'error',
          title: error.error.Message
        });
      }
      else {
        this.categories = undefined;
        this.notFound = error.status === 404;
      }
    });
  }

  delete(idCategory: number) {
    this._service.DeleteCategory(idCategory)
      .subscribe( resp => {
        if (resp.Result === Result.SuccessfulOperation) {
          this._toasterService.pop({
            type: 'success',
            title: 'Categoría eliminada'
          });
          this.ngOnInit();
        }
      }, error => {
        console.log(error);
        this._toasterService.pop({
          type: 'error',
          title: 'Ocurrió un error al eliminar la categoría'
        });
      })
  }

}
