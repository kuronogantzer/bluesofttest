import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {Result} from "../../../models/LibraryResponse";
import {Book} from "../../../models/Book";
import {BookService} from "../../../services/book.service";
import {Category} from "../../../models/Category";
import {Author} from "../../../models/Author";
import {CategoryService} from "../../../services/category.service";
import {AuthorService} from "../../../services/author.service";
import {NgbTypeahead} from "@ng-bootstrap/ng-bootstrap";
import {Observable, Subject, merge} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, map} from "rxjs/operators";

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styles: []
})
export class BookDetailComponent implements OnInit {

  private bookForm: FormGroup;
  private edit: boolean;
  private book: Book;
  private validated: boolean;
  private categories: Category[];
  private authors: Author[];

  constructor(private _service: BookService,
              private _categoryService: CategoryService,
              private _authorService: AuthorService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _formBuilder: FormBuilder,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this.bookForm = this._formBuilder.group({
      Name: new FormControl('', Validators.required),
      ISBN: new FormControl('', Validators.required),
      Author: new FormGroup({
        Id: new FormControl('', Validators.required)
      }),
      Category: new FormGroup({
        Id: new FormControl('', Validators.required)
      })
    });

    this._categoryService.GetCategories().subscribe( resp => {
      if (resp.Result == Result.SuccessfulOperation) {
        this.categories = resp.Data;
      }
    });

    this._authorService.GetAuthors().subscribe(resp => {
      if (resp.Result == Result.SuccessfulOperation) {
        this.authors = resp.Data;
      }
    });

    this._activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.edit = true;
        this._service.GetBookById(+params['id'])
          .subscribe(result => {
            this.book = result.Data;
            this.bookForm.patchValue(this.book);
            let selectCategory = document.getElementById('category');
            selectCategory.setAttribute("value", this.book.Category.Name);
            let selectAthor = document.getElementById('author');
            selectAthor.setAttribute("value", this.book.Author.Name + " " + this.book.Author.LastName);
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'No se pudo obtener la información del libro'
            });
          });
      }
    });
  }

  save() {
    if (this.bookForm.valid) {
      this.book = this.edit ? this.book : new Book();
      this.book.Name = this.bookForm.value.Name;
      this.book.ISBN = this.bookForm.value.ISBN;
      this.book.CategoryId = this.bookForm.value.Category.Id;
      this.book.AuthorId = this.bookForm.value.Author.Id;
      if (this.edit) {
        this._service.UpdateBook(this.book)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Libro actualizado'
              });
              this._router.navigate(['/libros']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al actualizar el libro.'
            });
          });
      } else {
        this._service.CreateBook(this.book)
          .subscribe(result => {
            if (result.Result == Result.SuccessfulOperation) {
              this._toasterService.pop({
                type: 'success',
                title: 'Libro creado'
              });
              this._router.navigate(['/libros']);
            }
          }, error => {
            console.log(error);
            this._toasterService.pop({
              type: 'error',
              title: 'Ocurrió un error al crear el libro'
            });
            this._router.navigate(['/libros']);
          });
      }
    }
    else {
      this.validated = true
    }
  }

  clear() {
    this.bookForm.reset();
  }

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focusCategory$ = new Subject<string>();
  clickCategory$ = new Subject<string>();

  searchCategory = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickCategory$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusCategory$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.categories
        : this.categories.filter(v => v.Name.toLowerCase().indexOf(term.toLowerCase()) > -1)).map(category => category.Name).slice(0, 10))
    );
  };

  focusAuthor$ = new Subject<string>();
  clickAuthor$ = new Subject<string>();

  searchAuthor = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickAuthor$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusAuthor$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.authors
        : this.authors.filter(v =>
          v.Name.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
          v.LastName.toLowerCase().indexOf(term.toLowerCase()) > -1)).map(author => author.Name + " " + author.LastName).slice(0, 10))
    );
  }

  categorySelected(event) {
    var category: Category = this.categories.find(c => c.Name === event.item);
    this.bookForm.controls['Category'].patchValue({Id: category.Id});
  }

  authorSelected(event) {
    var author: Author = this.authors.find(a => a.Name + " " + a.LastName === event.item);
    this.bookForm.controls['Author'].patchValue({Id: author.Id});
  }
}
