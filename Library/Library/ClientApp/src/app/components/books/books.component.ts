import {Component, OnInit} from '@angular/core';
import {Result} from "../../models/LibraryResponse";
import {ToasterService} from "angular2-toaster";
import {faEdit, faSearch, faTrash} from '@fortawesome/free-solid-svg-icons';
import {Book} from "../../models/Book";
import {BookService} from "../../services/book.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Filters} from "../../models/Filters";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styles: []
})
export class BooksComponent implements OnInit {

  private books: Book[];
  private notFound: boolean;
  private editIcon = faEdit;
  private deleteIcon = faTrash;
  private searchIcon = faSearch;
  private filtersForm: FormGroup;
  private filters: Filters;

  constructor(private _service: BookService,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    this.filtersForm = new FormGroup({
      BookName: new FormControl(''),
      CategoryName: new FormControl(''),
      AuthorName: new FormControl('')
    });

    this._service.GetBooks().subscribe( result => {
      if (result.Result == Result.SuccessfulOperation)
        this.books = result.Data;
      else {
        this._toasterService.pop({
          type: 'error',
          title: result.Message
        });
      }
    }, error => {
      if (error.status !== 404) {
        this._toasterService.pop({
          type: 'error',
          title: error.error.Message
        });
      }
      else {
        this.books = undefined;
        this.notFound = error.status === 404;
      }
    });
  }

  delete(idBook: number) {
    this._service.DeleteBook(idBook)
      .subscribe( resp => {
        if (resp.Result === Result.SuccessfulOperation) {
          this._toasterService.pop({
            type: 'success',
            title: 'Libro eliminado'
          });
          this.ngOnInit();
        }
      }, error => {
        console.log(error);
        this._toasterService.pop({
          type: 'error',
          title: 'Ocurrió un error al eliminar el libro'
        });
      })
  }

  searchBooks() {
    if (this.filtersForm.dirty)
    {
      this.filters = new Filters();
      this.filters.BookName = this.filtersForm.value.BookName;
      this.filters.AuthorName = this.filtersForm.value.AuthorName;
      this.filters.CategoryName = this.filtersForm.value.CategoryName;
      this._service.GetFilteredBooks(this.filters)
        .subscribe( resp => {
          if (resp.Result == Result.SuccessfulOperation) {
            this.books = resp.Data;
          }
        }, error => {
          console.log(error);
          if (error.status === 404)
          {
            this._toasterService.pop({
              type: 'error',
              title: 'No se encontraron libros con estos filtros'
            });
          }
        });
    }

  }
}
