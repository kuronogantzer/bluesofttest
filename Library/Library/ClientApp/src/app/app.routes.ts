﻿import { Routes, RouterModule } from "@angular/router";
import {HomeComponent} from "./components/home/home.component";
import {CategoriesComponent} from "./components/categories/categories.component";
import {CategoryDetailComponent} from "./components/categories/category-detail/category-detail.component";
import {AuthorsComponent} from "./components/authors/authors.component";
import {AuthorDetailComponent} from "./components/authors/author-detail/author-detail.component";
import {BookDetailComponent} from "./components/books/book-detail/book-detail.component";
import {BooksComponent} from "./components/books/books.component";

const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'categorias', component: CategoriesComponent },
  { path: 'categorias/detalle', component: CategoryDetailComponent },
  { path: 'categorias/detalle/:id', component: CategoryDetailComponent },
  { path: 'autores', component: AuthorsComponent },
  { path: 'autores/detalle', component: AuthorDetailComponent },
  { path: 'autores/detalle/:id', component: AuthorDetailComponent },
  { path: 'libros', component: BooksComponent },
  { path: 'libros/detalle', component: BookDetailComponent },
  { path: 'libros/detalle/:id', component: BookDetailComponent },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
