import { Component } from '@angular/core';
import {ToasterConfig} from "angular2-toaster";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  toastConfig = new ToasterConfig({
                        positionClass: 'toast-bottom-center',
                        showCloseButton: false,
                        timeout: 5000});
}
