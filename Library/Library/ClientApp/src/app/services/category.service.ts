import {Inject, Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from "../models/Category";
import { Observable } from "rxjs";
import {LibraryResponse} from "../models/LibraryResponse";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private _baseUrl: string;

  constructor(private _http:HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl + "category";
  }

  GetCategories() : Observable<LibraryResponse<Category[]>>{
    return this._http.get<LibraryResponse<Category[]>>(this._baseUrl);
  }

  GetCategoryById(idCategory: number) : Observable<LibraryResponse<Category>> {
    return this._http.get<LibraryResponse<Category>>(this._baseUrl + "/" + idCategory.toString());
  }

  CreateCategory(category: Category) : Observable<LibraryResponse<Category>> {
    return this._http.post<LibraryResponse<Category>>(this._baseUrl, category);
  }

  UpdateCategory(category: Category) : Observable<LibraryResponse<Category>> {
    return this._http.put<LibraryResponse<Category>>(this._baseUrl, category);
  }

  DeleteCategory(idCategory: number) : Observable<LibraryResponse<boolean>> {
    return this._http.delete<LibraryResponse<boolean>>(this._baseUrl + "/" + idCategory.toString());
  }
}
