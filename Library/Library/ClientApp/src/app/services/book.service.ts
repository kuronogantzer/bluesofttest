import {Inject, Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import {LibraryResponse} from "../models/LibraryResponse";
import {Book} from "../models/Book";
import {Filters} from "../models/Filters";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private _baseUrl: string;

  constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl + "book";
  }

  GetBooks() : Observable<LibraryResponse<Book[]>> {
    return this._http.get<LibraryResponse<Book[]>>(this._baseUrl);
  }

  GetBookById(idBook: number) : Observable<LibraryResponse<Book>> {
    return this._http.get<LibraryResponse<Book>>(this._baseUrl + "/" + idBook.toString());
  }

  CreateBook(book: Book) : Observable<LibraryResponse<Book>> {
    return this._http.post<LibraryResponse<Book>>(this._baseUrl, book);
  }

  UpdateBook(book: Book) : Observable<LibraryResponse<Book>> {
    return this._http.put<LibraryResponse<Book>>(this._baseUrl, book);
  }

  DeleteBook(idBook: number) : Observable<LibraryResponse<boolean>> {
    return this._http.delete<LibraryResponse<boolean>>(this._baseUrl + "/" + idBook.toString());
  }

  GetFilteredBooks(filters: Filters) : Observable<LibraryResponse<Book[]>> {
    return this._http.post<LibraryResponse<Book[]>>(this._baseUrl + "/filter", filters);
  }
}
