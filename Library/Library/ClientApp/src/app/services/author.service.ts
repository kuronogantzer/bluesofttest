import {Inject, Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import {LibraryResponse} from "../models/LibraryResponse";
import {Author} from "../models/Author";

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  private _baseUrl: string;

  constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl + "author";
  }

  GetAuthors() : Observable<LibraryResponse<Author[]>> {
    return this._http.get<LibraryResponse<Author[]>>(this._baseUrl);
  }

  GetAuthorById(idAuthor: number) : Observable<LibraryResponse<Author>> {
    return this._http.get<LibraryResponse<Author>>(this._baseUrl + "/" + idAuthor.toString());
  }

  CreateAuthor(author: Author) : Observable<LibraryResponse<Author>> {
    return this._http.post<LibraryResponse<Author>>(this._baseUrl, author);
  }

  UpdateAuthor(author: Author) : Observable<LibraryResponse<Author>> {
    return this._http.put<LibraryResponse<Author>>(this._baseUrl, author);
  }

  DeleteAuthor(idAuthor: number) : Observable<LibraryResponse<boolean>> {
    return this._http.delete<LibraryResponse<boolean>>(this._baseUrl + "/" + idAuthor.toString());
  }
}
