﻿export class Category {
  Id: number;
  Name: string;
  Description: string;
  Books: any[];
}
