﻿import {Category} from "./Category";
import {Author} from "./Author";

export class Book {
  Id: number;
  Name: string;
  ISBN: string;
  CategoryId: number;
  AuthorId: number;

  Category: Category;
  Author: Author;
}
