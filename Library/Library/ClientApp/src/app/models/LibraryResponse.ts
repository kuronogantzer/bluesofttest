﻿export class LibraryResponse<T> {
  Result: Result;
  Message: string;
  Data: T;
}

export enum Result {
  SuccessfulOperation = "SuccessfulOperation",
  DataNotFound = "DataNotFound",
  IncorrectData = "IncorrectData",
  ServerError = "ServerError"
}
