﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LibraryBusiness;
using LibraryEntities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Library.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly ILogger<AuthorController> _logger;
        private readonly AuthorBusiness _business;

        public AuthorController(ILogger<AuthorController> logger, AuthorBusiness business)
        {
            _logger = logger;
            _business = business;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAuthors()
        {
            try
            {
                var results = await _business.GetAuthors();
                LibraryResponse response;
                if (!results.Any())
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't authors in database."
                    };
                    return NotFound(response);
                }

                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = results
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpGet("{idAuthor}")]
        public async Task<IActionResult> GetAuthorById(int idAuthor)
        {
            try
            {
                var result = await _business.GetAuthorById(idAuthor);
                LibraryResponse response;
                if (result == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't any author with id: " + idAuthor.ToString()
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAuthor([FromBody] Author author)
        {
            try
            {
                LibraryResponse response;
                if (author == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The author's structure isn't correct."
                    };
                    return BadRequest(response);
                }

                var authorCreated = await _business.CreateAuthor(author);
                if (authorCreated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "Something was wrong creating author"
                    };
                    _logger.LogError("Something was wrong creating author");
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = authorCreated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAuthor([FromBody] Author author)
        {
            try
            {
                LibraryResponse response;
                if (author == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The author's structure isn't correct."
                    };
                    return BadRequest(response);
                }

                var authorUpdated = await _business.UpdateAuthor(author);
                if (authorUpdated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.ServerError,
                        Message = "Something was wrong updating author"
                    };
                    _logger.LogError("Something was wrong updating author");
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = authorUpdated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
        
        [HttpDelete("{idAuthor}")]
        public async Task<IActionResult> DeleteAuthor(int idAuthor)
        {
            try
            {
                var result = await _business.DeleteAuthor(idAuthor);
                LibraryResponse response;
                if (!result)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.ServerError,
                        Message = "Something was wrong deleting author: " + idAuthor.ToString()
                    };
                    _logger.LogError("Something was wrong deleting author: " + idAuthor.ToString());
                    return StatusCode(500, response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
    }
}