﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LibraryBusiness;
using LibraryEntities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Library.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly CategoryBusiness _business;

        public CategoryController(ILogger<CategoryController> logger, CategoryBusiness business)
        {
            _logger = logger;
            _business = business;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            try
            {
                LibraryResponse response;
                var result = await _business.GetCategories();
                if (!result.Any())
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't categories in database"
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpGet("{idCategory}")]
        public async Task<IActionResult> GetCategoryById(int idCategory)
        {
            try
            {
                LibraryResponse response;
                var result = await _business.GetCategoryById(idCategory);
                if (result == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't any category with id: " + idCategory.ToString()
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromBody] Category category)
        {
            try
            {
                LibraryResponse response;
                if (category == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The category's structure isn't correct."
                    };
                    return BadRequest(response);
                }

                var categoryCreated = await _business.CreateCategory(category);
                if (categoryCreated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "Something was wrong creating author"
                    };
                    _logger.LogError("Something was wrong creating author");
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = categoryCreated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
        
        [HttpPut]
        public async Task<IActionResult> UpdateCategory([FromBody] Category category)
        {
            try
            {
                LibraryResponse response;
                if (category == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The category's structure isn't correct."
                    };
                    return BadRequest(response);
                }

                var categoryUpdated = await _business.UpdateCategory(category);
                if (categoryUpdated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "Something was wrong updating author"
                    };
                    _logger.LogError("Something was wrong updating author");
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = categoryUpdated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpDelete("{idCategory}")]
        public async Task<IActionResult> DeleteCategory(int idCategory)
        {
            try
            {
                LibraryResponse response;
                var result = await _business.DeleteCategory(idCategory);
                if (!result)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.ServerError,
                        Message = "Something was wrong deleting author: " + idCategory.ToString()
                    };
                    _logger.LogError("Something was wrong deleting author: " + idCategory.ToString());
                    return StatusCode(500, response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
    }
}