﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LibraryBusiness;
using LibraryEntities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Library.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly ILogger<BookController> _logger;
        private readonly BookBusiness _business;

        public BookController(ILogger<BookController> logger, BookBusiness business)
        {
            _logger = logger;
            _business = business;
        }

        [HttpGet]
        public async Task<IActionResult> GetBooks()
        {
            try
            {
                LibraryResponse response;
                var result = await _business.GetBooks();
                if (!result.Any())
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't books in database."
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpGet("{idBook}")]
        public async Task<IActionResult> GetBookById(int idBook)
        {
            try
            {
                LibraryResponse response;
                var result = await _business.GetBookById(idBook);
                if (result == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't any author with id: " + idBook.ToString()
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateBook([FromBody] Book book)
        {
            try
            {
                LibraryResponse response;
                if (book == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The book's structure isn't correct."
                    };
                }

                var bookCreated = await _business.CreateBook(book);
                if (bookCreated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "Something was wrong creating book"
                    };
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = bookCreated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
        
        [HttpPut]
        public async Task<IActionResult> UpdateBook([FromBody] Book book)
        {
            try
            {
                LibraryResponse response;
                if (book == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The book's structure isn't correct."
                    };
                }

                var bookCreated = await _business.UpdateBook(book);
                if (bookCreated == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "Something was wrong updating book"
                    };
                    return BadRequest(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = bookCreated
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpDelete("{idBook}")]
        public async Task<IActionResult> DeleteBook(int idBook)
        {
            try
            {
                LibraryResponse response;
                var result = await _business.DeleteBook(idBook);
                if (!result)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't any author with id: " + idBook.ToString()
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = result
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }

        [HttpPost("filter")]
        public async Task<IActionResult> GetFilteredBooks([FromBody] Filters filters)
        {
            try
            {
                LibraryResponse response;
                if (filters == null)
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.IncorrectData,
                        Message = "The filter's structure isn't correct."
                    };
                    return BadRequest(response);
                }
                var results = await _business.GetFilteredBooks(filters);
                if (!results.Any())
                {
                    response = new LibraryResponse()
                    {
                        Result = ResultType.DataNotFound,
                        Message = "There isn't books with this filters."
                    };
                    return NotFound(response);
                }
                response = new LibraryResponse()
                {
                    Result = ResultType.SuccessfulOperation,
                    Data = results
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                return StatusCode(500, e.ToString());
            }
        }
    }
}