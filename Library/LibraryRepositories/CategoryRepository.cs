﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryEntities;
using LibraryEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryRepositories
{
    public class CategoryRepository
    {
        private readonly LibraryContext _context;

        public CategoryRepository(LibraryContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _context.Category.ToListAsync();
        }

        public async Task<Category> GetCategoryById(int idCategory)
        {
            return await _context.Category.FirstOrDefaultAsync(c => c.Id == idCategory);
        }

        public async Task<Category> CreateCategory(Category category)
        {
            _context.Category.Add(category);
            await _context.SaveChangesAsync();
            return category;
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            var previousCategory = await _context.Category.FirstOrDefaultAsync(c => c.Id == category.Id);
            if (previousCategory == null) return null;
            previousCategory.Description = category.Description;
            previousCategory.Name = category.Name;
            await _context.SaveChangesAsync();
            return category;
        }

        public async Task<bool> DeleteCategory(int idCategory)
        {
            var category = await _context.Category.FirstOrDefaultAsync(c => c.Id == idCategory);
            if (category == null) return false;
            _context.Category.Remove(category);
            return await _context.SaveChangesAsync() == 1;
        }
    }
}