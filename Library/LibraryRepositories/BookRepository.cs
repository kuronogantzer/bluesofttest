﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryEntities;
using LibraryEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryRepositories
{
    public class BookRepository
    {
        private readonly LibraryContext _context;

        public BookRepository(LibraryContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Book>> GetBooks()
        {
            return await _context.Book
                .Include(b => b.Author)
                .Include(b => b.Category).ToListAsync();
        }

        public async Task<Book> GetBookById(int idBook)
        {
            return await _context.Book
                .Include(b => b.Author)
                .Include(b => b.Category).FirstOrDefaultAsync(b => b.Id == idBook);
        }

        public async Task<Book> CreateBook(Book book)
        {
            _context.Book.Add(book);
            await _context.SaveChangesAsync();
            return book;
        }

        public async Task<Book> UpdateBook(Book book)
        {
            var previousBook = await _context.Book.FirstOrDefaultAsync(b => b.Id == book.Id);
            if (previousBook == null) return null;
            previousBook.Name = book.Name;
            previousBook.ISBN = book.ISBN;
            previousBook.AuthorId = book.AuthorId;
            previousBook.CategoryId = book.CategoryId;
            await _context.SaveChangesAsync();
            return book;
        }

        public async Task<bool> DeleteBook(int idBook)
        {
            var book = await _context.Book.FirstOrDefaultAsync(b => b.Id == idBook);
            if (book == null) return false;
            _context.Book.Remove(book);
            return await _context.SaveChangesAsync() == 1;
        }

        public async Task<IEnumerable<Book>> GetFilteredBooks(Filters filters)
        {
            return await _context.Book
                .Include(b => b.Author)
                .Include(b => b.Category).Where(b =>
                (string.IsNullOrEmpty(filters.BookName) || b.Name.ToLower().Contains(filters.BookName.ToLower()))
                && (string.IsNullOrEmpty(filters.AuthorName) || (b.Author.Name + " " + b.Author.LastName).ToLower().Contains(filters.AuthorName.ToLower())) 
                && (string.IsNullOrEmpty(filters.CategoryName) || b.Category.Name.ToLower().Contains(filters.CategoryName.ToLower()))).ToListAsync();
        }
    }
}