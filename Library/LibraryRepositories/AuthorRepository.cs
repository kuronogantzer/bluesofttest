﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryEntities;
using LibraryEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryRepositories
{
    public class AuthorRepository
    {
        private readonly LibraryContext _context;

        public AuthorRepository(LibraryContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Author>> GetAuthors()
        {
            return await _context.Author.ToListAsync();
        }

        public async Task<Author> GetAuthorById(int idAuthor)
        {
            return await _context.Author.FirstOrDefaultAsync(a => a.Id == idAuthor);
        }

        public async Task<Author> CreateAuthor(Author author)
        {
            _context.Author.Add(author);
            await _context.SaveChangesAsync();
            return author;
        }

        public async Task<Author> UpdateAuthor(Author author)
        {
            var previousAuthor = _context.Author.FirstOrDefault(a => a.Id == author.Id);
            if (previousAuthor == null) return null;
            previousAuthor.Name = author.Name;
            previousAuthor.LastName = author.LastName;
            previousAuthor.BirthDate = author.BirthDate;
            await _context.SaveChangesAsync();
            return author;
        }

        public async Task<bool> DeleteAuthor(int idAuthor)
        {
            var author = await _context.Author.FirstOrDefaultAsync(a => a.Id == idAuthor);
            if (author == null) return false;
            _context.Author.Remove(author);
            return await _context.SaveChangesAsync() == 1;
        }
    }
}