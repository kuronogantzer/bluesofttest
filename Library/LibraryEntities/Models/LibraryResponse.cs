﻿using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace LibraryEntities.Models
{
    public class LibraryResponse
    {
        public ResultType Result { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }

    }

    public enum ResultType
    {
        SuccessfulOperation,
        DataNotFound,
        IncorrectData,
        ServerError
    }
}