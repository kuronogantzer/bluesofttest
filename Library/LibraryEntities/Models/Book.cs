﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryEntities.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ISBN { get; set; }
        public int AuthorId { get; set; }
        public int CategoryId { get; set; }
        
        [ForeignKey("AuthorId")]
        public Author Author { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}