﻿namespace LibraryEntities.Models
{
    public class Filters
    {
        public string BookName { get; set; }
        public string CategoryName { get; set; }
        public string AuthorName { get; set; }
    }
}