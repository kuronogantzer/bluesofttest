﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using LibraryEntities.Models;
using LibraryRepositories;

namespace LibraryBusiness
{
    public class AuthorBusiness
    {
        private readonly AuthorRepository _repository;

        public AuthorBusiness(AuthorRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Author>> GetAuthors()
        {
            return await _repository.GetAuthors();
        }

        public async Task<Author> GetAuthorById(int idAuthor)
        {
            return idAuthor > 0 ? await _repository.GetAuthorById(idAuthor) : null;
        }

        public async Task<Author> CreateAuthor(Author author)
        {
            if (author.BirthDate == new DateTime() || string.IsNullOrEmpty(author.Name) ||
                string.IsNullOrEmpty(author.LastName))
                return null;
            return await _repository.CreateAuthor(author);
        }

        public async Task<Author> UpdateAuthor(Author author)
        {
            if (author.BirthDate == new DateTime() || string.IsNullOrEmpty(author.Name) ||
                string.IsNullOrEmpty(author.LastName) || author.Id <= 0)
                return null;
            return await _repository.UpdateAuthor(author);
        }

        public async Task<bool> DeleteAuthor(int idAuthor)
        {
            return idAuthor > 0 && await _repository.DeleteAuthor(idAuthor);
        }
    }
}