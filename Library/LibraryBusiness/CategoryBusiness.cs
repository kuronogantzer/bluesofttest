﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryEntities.Models;
using LibraryRepositories;

namespace LibraryBusiness
{
    public class CategoryBusiness
    {
        private readonly CategoryRepository _repository;

        public CategoryBusiness(CategoryRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _repository.GetCategories();
        }

        public async Task<Category> GetCategoryById(int idCategory)
        {
            return idCategory > 0 ? await _repository.GetCategoryById(idCategory) : null;
        }

        public async Task<Category> CreateCategory(Category category)
        {
            if (string.IsNullOrEmpty(category.Name) || string.IsNullOrEmpty(category.Description))
                return null;
            return await _repository.CreateCategory(category);
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            if (category.Id <= 0 || string.IsNullOrEmpty(category.Name) || string.IsNullOrEmpty(category.Description))
                return null;
            return await _repository.UpdateCategory(category);
        }

        public async Task<bool> DeleteCategory(int idCategory)
        {
            return idCategory > 0 && await _repository.DeleteCategory(idCategory);
        }
    }
}