﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryEntities.Models;
using LibraryRepositories;

namespace LibraryBusiness
{
    public class BookBusiness
    {
        private readonly BookRepository _repository;

        public BookBusiness(BookRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Book>> GetBooks()
        {
            return await _repository.GetBooks();
        }

        public async Task<Book> GetBookById(int idBook)
        {
            return idBook > 0 ? await _repository.GetBookById(idBook) : null;
        }

        public async Task<Book> CreateBook(Book book)
        {
            if (book.AuthorId <= 0 || book.CategoryId <= 0 || string.IsNullOrEmpty(book.Name) ||
                string.IsNullOrEmpty(book.ISBN))
                return null;
            return await _repository.CreateBook(book);
        }
        
        public async Task<Book> UpdateBook(Book book)
        {
            if (book.AuthorId <= 0 || book.CategoryId <= 0 || string.IsNullOrEmpty(book.Name) ||
                string.IsNullOrEmpty(book.ISBN) || book.Id <= 0)
                return null;
            return await _repository.UpdateBook(book);
        }

        public async Task<bool> DeleteBook(int idBook)
        {
            return idBook > 0 && await _repository.DeleteBook(idBook);
        }

        public async Task<IEnumerable<Book>> GetFilteredBooks(Filters filters)
        {
            return await _repository.GetFilteredBooks(filters);
        }
    }
}